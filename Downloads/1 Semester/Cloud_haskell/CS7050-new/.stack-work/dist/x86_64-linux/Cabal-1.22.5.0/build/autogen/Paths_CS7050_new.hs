module Paths_CS7050_new (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/zhao/Downloads/1 Semester/Cloud_haskell/newrepository/Downloads/1 Semester/Cloud_haskell/CS7050-new/.stack-work/install/x86_64-linux/lts-6.3/7.10.3/bin"
libdir     = "/home/zhao/Downloads/1 Semester/Cloud_haskell/newrepository/Downloads/1 Semester/Cloud_haskell/CS7050-new/.stack-work/install/x86_64-linux/lts-6.3/7.10.3/lib/x86_64-linux-ghc-7.10.3/CS7050-new-0.1.0.0-16wBpyWu9bk8QxvktA4Izd"
datadir    = "/home/zhao/Downloads/1 Semester/Cloud_haskell/newrepository/Downloads/1 Semester/Cloud_haskell/CS7050-new/.stack-work/install/x86_64-linux/lts-6.3/7.10.3/share/x86_64-linux-ghc-7.10.3/CS7050-new-0.1.0.0"
libexecdir = "/home/zhao/Downloads/1 Semester/Cloud_haskell/newrepository/Downloads/1 Semester/Cloud_haskell/CS7050-new/.stack-work/install/x86_64-linux/lts-6.3/7.10.3/libexec"
sysconfdir = "/home/zhao/Downloads/1 Semester/Cloud_haskell/newrepository/Downloads/1 Semester/Cloud_haskell/CS7050-new/.stack-work/install/x86_64-linux/lts-6.3/7.10.3/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "CS7050_new_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "CS7050_new_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "CS7050_new_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "CS7050_new_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "CS7050_new_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
