How to run the repository <br />

(1) pre-requirement <br />
	cd ./newrepository/Downloads/1\ Semester/Cloud_haskell/CS7050-new/	<br />
	stack build	<br />
(2) start worker nodes <br />
	stack exec CS7050-new-exe -- worker 0.0.0.0 (port number)	<br />
	For example,
	stack exec CS7050-new-exe -- worker 0.0.0.0 8000 <br />
	stack exec CS7050-new-exe -- worker 0.0.0.0 8001 <br />
	//this step can run many nodes in different command windows	<br />
(3) start manager node	<br />
	time stack exec CS7050-new-exe -- manager 0.0.0.0 (port number)	<br />
	//start a new command window	<br />
