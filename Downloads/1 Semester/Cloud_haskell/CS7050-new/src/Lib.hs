{-# LANGUAGE TemplateHaskell #-}
module Lib
    ( mainFunction
    ) where

import Control.Exception (evaluate)
import Formatting
import Formatting.Clock
import System.Clock
import Text.Printf
import System.CPUTime (getCPUTime)
import Control.Monad
import Control.Parallel.Strategies (NFData, rdeepseq)
import Control.DeepSeq
import System.TimeIt
import System.Environment (getArgs)
import System.Process
import System.Directory (doesDirectoryExist)
import Control.Distributed.Process
import Control.Distributed.Process.Node (initRemoteTable, runProcess)
import Control.Distributed.Process.Backend.SimpleLocalnet
import Control.Distributed.Process.Closure
import Control.Monad (replicateM_, forever, forM_)
import Data.List
import Data.List.Split
import Data.String.Utils
import Argon
import qualified Pipes.Prelude as P
import Pipes
import Pipes.Safe (runSafeT)
import System.IO.Silently
import MapReduce

time :: (Num t, NFData t) => t -> IO ()
time y = do
  let trials = 10^6
  start <- getCPUTime
  replicateM_ trials $ do
      x <- evaluate $ 1 + y
      rdeepseq x `seq` return ()
  end   <- getCPUTime
  let diff = (fromIntegral (end - start)) / (10^12)
  putStrLn $ "avg seconds: " ++
    (show (diff / fromIntegral trials))
  return ()

remotable ['mapperProcess]


mapRemoteTable :: RemoteTable
mapRemoteTable = Lib.__remoteTable initRemoteTable


manager :: Backend -> [NodeId] -> Process ()
manager backend workers = do
  liftIO . putStrLn $ "Workers are working in the following port: " ++ show workers
  let repositaries = ["https://github.com/tensorflow/haskell.git", "https://github.com/pietervdvn/Haskell.git", "https://github.com/pubnub/haskell.git", "https://github.com/haskell/hackage-server.git", "https://github.com/ryanpbrewster/haskell.git", "https://github.com/justinethier/husk-scheme.git"]
  --let repositaries = ["https://github.com/justinethier/husk-scheme.git"]
  responses <- distrMapReduceAndGetResponses repositaries workers [] []
  liftIO $ mapM (\(r,u) -> putStrLn $ "\n\n\n\n" ++ u ++ " :\n\n\n" ++  r) responses
  return ()


distrMapReduceAndGetResponses :: [String] -> [NodeId] -> [NodeId] -> [(String,String)] -> Process [(String,String)]
distrMapReduceAndGetResponses [] freeWorkers [] responses = return responses
distrMapReduceAndGetResponses repositaries freeWorkers busyWorkers responses = do
  (restRepositaries, newBusyWorkers, newFreeWorkers) <- distrMapReduce repositaries freeWorkers []
  m <- expectTimeout 30000000
  case m of
    Just (worker, url, resp) -> distrMapReduceAndGetResponses restRepositaries (worker:newFreeWorkers) (delete worker (newBusyWorkers ++ busyWorkers)) ((resp,url):responses)


distrMapReduce :: [String] -> [NodeId] -> [NodeId] -> Process ([String], [NodeId], [NodeId])
distrMapReduce [] workers newBusyWorkers = return ([], newBusyWorkers, workers)
distrMapReduce repositaries [] newBusyWorkers = return (repositaries, newBusyWorkers, [])
distrMapReduce (repo:repositaries) (oneWorker:workers) newBusyWorkers = do
  managerPid <- getSelfPid
  _ <- spawn oneWorker $ $(mkClosure 'mapperProcess) (managerPid, oneWorker, repo :: String)
  distrMapReduce repositaries workers (oneWorker:newBusyWorkers)

mainFunction :: IO ()
mainFunction = do
  args <- getArgs
  case args of
    ["manager", host, port] -> do
      putStrLn "Manager is starting"
      backend <- initializeBackend host port mapRemoteTable
      startMaster backend (manager backend)
      putStrLn "*************Time calculate************"
      time (3+7 :: Int)
      putStrLn "Done."
    ["worker", host, port] -> do
      putStrLn  "worker is starting"
      backend <- initializeBackend host port mapRemoteTable
      startSlave backend
      putStrLn "*************Time calculate************"
      time (3+7 :: Int)
      putStrLn "Done."
