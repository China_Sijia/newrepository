module WorkStealing where

import Control.Monad
import Control.Distributed.Process
import Control.Distributed.Process.Closure

slave :: (ProcessId, NodeId, String) -> Process ()
slave (master, workQueue, slaveId, url) = do
    us <- getSelfPid
    go us
  where
    go us = do
      -- Ask the queue for work 
      send workQueue us
      
      liftIO ( putStrLn $ "Starting slave : " ++ (show slaveId) ++ " with parameter: " ++ url)
	 let repoName = last $ splitOn "/" url
	 gitRepoExists <- liftIO $ doesDirectoryExist ("/tmp/" ++ repoName)
	 if not gitRepoExists then do
	    liftIO $ callProcess "/usr/bin/git" ["clone", url, "/tmp/" ++ repoName]
	 else do
	      liftIO $ putStrLn "Repository existing. Don't need to download."
	 let conf = (Config 6 [] [] [] Colored)
	 let source = allFiles ("/tmp/" ++ repoName)
		      >-> P.mapM (liftIO . analyze conf)
		      >-> P.map (filterResults conf)
		      >-> P.filter filterNulls
	 liftIO $ putStrLn $ "Launching analyse for " ++ url
	 (output, _) <- liftIO $ capture $ runSafeT $ runEffect $ exportStream conf source
	 liftIO ( putStrLn $ "End of slave : " ++ (show slaveId) ++ " with parameter: " ++ url)      

      -- If there is work, do it, otherwise terminate 
      receiveWait 
        [ match $ \n  -> send master $ (slaveId, url, output) >> go us
        , match $ \() -> return ()
        ]

remotable ['slave]

rtable :: RemoteTable
rtable = WorkStealing.__remoteTable initRemoteTable 

masterfunction :: Backend -> [NodeId] -> Process ()
masterfunction backend slaves = do
  liftIO . putStrLn $ "Slave are working in the following port: " ++ show slaves
  let repositaries = ["https://github.com/justinethier/husk-scheme.git"]
  responses <- distrMapReduceAndGetResponses repositaries slaves [] []
  liftIO $ mapM (\(r,u) -> putStrLn $ "\n\n\n\n" ++ u ++ " :\n\n\n" ++  r) responses
  return ()

master :: Backend -> [NodeId] -> Process () 
master n slaves = do
  us <- getSelfPid

  workQueue <- spawnLocal $ do
    -- Reply with the next bit of work to be done 
    forM_ [1 .. n] $ \m -> do
      them <- expect 
      send them m 

    -- Once all the work is done, tell the slaves to terminate
    forever $ do
      pid <- expect
      send pid ()

  -- Start slave processes 
  forM_ slaves $ \nid -> spawn nid ($(mkClosure 'slave) (us, workQueue))

main :: IO ()
main = do
  args <- getArgs

  case args of
    ["master", host, port, n] -> do
      backend <- initializeBackend host port rtable 
      startMaster backend $ \slaves -> do
        result <- WorkStealing.master (read n) slaves
        liftIO $ print result 
    ["slave", host, port] -> do
      backend <- initializeBackend host port rtable 
      startSlave backend
