module MasterSlave where

import Control.Monad
import Control.Distributed.Process
import Control.Distributed.Process.Closure

slave :: (ProcessId, NodeId, String) -> Process ()
slave (master, slaveId, url) = do
  liftIO ( putStrLn $ "Starting slave : " ++ (show slaveId) ++ " with parameter: " ++ url)
    let repoName = last $ splitOn "/" url
    gitRepoExists <- liftIO $ doesDirectoryExist ("/tmp/" ++ repoName)
    if not gitRepoExists then do
      liftIO $ callProcess "/usr/bin/git" ["clone", url, "/tmp/" ++ repoName]
    else do
      liftIO $ putStrLn "Repository existing. Don't need to download."
    let conf = (Config 6 [] [] [] Colored)
    let source = allFiles ("/tmp/" ++ repoName)
                >-> P.mapM (liftIO . analyze conf)
                >-> P.map (filterResults conf)
                >-> P.filter filterNulls
    liftIO $ putStrLn $ "Launching analyse for " ++ url
    (output, _) <- liftIO $ capture $ runSafeT $ runEffect $ exportStream conf source
    liftIO ( putStrLn $ "End of slave : " ++ (show slaveId) ++ " with parameter: " ++ url)
  send master $ (slaveId, url, output)

remotable ['slave]

rtable :: RemoteTable
rtable = MasterSlave.__remoteTable initRemoteTable 

masterfunction :: Backend -> [NodeId] -> Process ()
masterfunction backend slaves = do
  liftIO . putStrLn $ "Slave are working in the following port: " ++ show slaves
  let repositaries = ["https://github.com/justinethier/husk-scheme.git"]
  responses <- distrMapReduceAndGetResponses repositaries slaves [] []
  liftIO $ mapM (\(r,u) -> putStrLn $ "\n\n\n\n" ++ u ++ " :\n\n\n" ++  r) responses
  return ()

data SpawnStrategy = SpawnSyncWithReconnect
                   | SpawnSyncNoReconnect
                   | SpawnAsync
  deriving (Show, Read)                   

master :: Backend -> SpawnStrategy -> [NodeId] -> Process ()
master n spawnStrategy slaves = do
  us <- getSelfPid

  -- Distribute 1 .. n amongst the slave processes 
  spawnLocal $ case spawnStrategy of 
    SpawnSyncWithReconnect ->
      forM_ (zip [1 .. n] (cycle slaves)) $ \(m, there) -> do
        them <- spawn there ($(mkClosure 'slave) (us, m))
        reconnect them
    SpawnSyncNoReconnect ->
      forM_ (zip [1 .. n] (cycle slaves)) $ \(m, there) -> do
        _them <- spawn there ($(mkClosure 'slave) (us, m))
        return ()
    SpawnAsync ->
      forM_ (zip [1 .. n] (cycle slaves)) $ \(m, there) -> do
        spawnAsync there ($(mkClosure 'slave) (us, m))
        _ <- expectTimeout 0 :: Process (Maybe DidSpawn)
        return ()

main :: IO ()
main = do
  args <- getArgs

  case args of
    ["master", host, port, strN, strSpawnStrategy] -> do
      backend <- initializeBackend host port rtable 
      n             <- evaluate $ read strN
      spawnStrategy <- evaluate $ read strSpawnStrategy
      startMaster backend $ \slaves -> do
        result <- MasterSlave.master n spawnStrategy slaves
        liftIO $ print result 
    ["slave", host, port] -> do
      backend <- initializeBackend host port rtable 
      startSlave backend
